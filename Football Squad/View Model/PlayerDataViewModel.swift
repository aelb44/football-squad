//
//  PlayerViewModel.swift
//  Football Squad
//
//  Created by ii on 13.05.20.
//  Copyright © 2020 Orga. All rights reserved.
//

import Foundation

struct PlayerDataViewModel {
    var name: String
    var id: Int
    var position: String?
    var number: Int?
    var club: String?
}

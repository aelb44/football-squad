//
//  PlayerListScreen.swift
//  Football Squad
//
//  Created by ii on 13.05.20.
//  Copyright © 2020 Orga. All rights reserved.
//

import SwiftUI
import CoreData

struct PlayerListScreen: View {
    
    @Environment(\.managedObjectContext) var managedObjectContext

    @FetchRequest(
        entity: Player.entity(),
        sortDescriptors: [
            NSSortDescriptor(
                keyPath: \Player.name,
                ascending: true
            )
        ]
    ) var players: FetchedResults<Player>
    var body: some View {
        List (players, id: \Player.id) { (player: Player) in
            Text(player.name ?? "<no name>")
        }
        .onAppear {
            self.players.forEach { player in
                self.managedObjectContext.delete(player)
            }
            try! self.managedObjectContext.save()
            
            let object1 = Player(context: self.managedObjectContext)
            object1.name = "Boating"
            object1.id = 1
            
            try! self.managedObjectContext.save()
        
        }
    }
}



//
//  PlayerRow.swift
//  Football Squad
//
//  Created by ii on 13.05.20.
//  Copyright © 2020 Orga. All rights reserved.
//

import SwiftUI

struct PlayerRow: View {
    var playerData: PlayerDataViewModel

    var body: some View {
        HStack {
            Text(playerData.name)
                .bold()
                
            
            
        }
    }
}


struct PlayerRow_Previews: PreviewProvider {
    static var previews: some View {
            PlayerRow(playerData: PlayerDataViewModel(name: "muster", id: 0)
            )
        .previewLayout(.fixed(width: 300, height: 70))
    }
}

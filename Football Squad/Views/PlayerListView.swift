//
//  PlayerListView.swift
//  Football Squad
//
//  Created by ii on 13.05.20.
//  Copyright © 2020 Orga. All rights reserved.
//

import SwiftUI

struct PlayerListView: View {
    let playerListData: [PlayerDataViewModel]
    
    var body: some View {
        List(playerListData, id: \PlayerDataViewModel.id) { (player: PlayerDataViewModel) in
            NavigationLink(destination: PlayerListScreen()) {
                PlayerRow(playerData: player)
            }
        }
    }
}

struct PlayerListView_Previews: PreviewProvider {
    static var previews: some View {
        PlayerListView(playerListData:
            [
                PlayerDataViewModel(
                    name: "Boating",
                    id: 1
                ),
                PlayerDataViewModel(
                    name: "Charles",
                    id: 2
                )
            ]
        )
        .previewLayout(.sizeThatFits)
    }
}
